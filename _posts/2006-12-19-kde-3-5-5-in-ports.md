---
title: 'KDE 3.5.5 in ports'
date: 2006-12-19 00:00:00 
layout: post
---

<p>KDE 3.5.5 has been committed to ports.</p><p>UPDATING:</p><pre>
20061219:
  AFFECTS: users of x11/kdebase3
  AUTHOR: kde@FreeBSD.org

  If you choose to enable the HAL backend for the media kioslave,
  you should enable D-Bus, HAL and PolicyKit during system startup.
  To do this, add the following lines to /etc/rc.conf:

  dbus_enable="YES"
  polkitd_enable="YES"
  hald_enable="YES"

  Notes / known problems about mounting devices in media:/ with HAL enabled:

  - Your user must be in group operator in order to be able to mount removable
    media.

  - Removable media drives (such as CD-ROM drives) *MUST NOT* be listed in
    /etc/fstab in order to be mountable.

  - If you mount a volume by double-clicking it, it may appear empty. Refresh
    the fileview in Konqueror to see the files.

  - Mounting floppies is currently not supported.

  You can still mount them in KDE if...

  - You have an fstab entry for your floppy pointing to a mountpoint
    owned by your user (i.e. somewhere in your homedir).

  - The vfs.usermount sysctl is set to 1.

  ...by creating a floppy device icon on your KDE desktop and selecting
  the above-mentioned fstab entry in the device dropdown list on the
  device tab.

  - Mounting volumes from fixed drives is prohibited for non-superusers
    by default.
</pre><p>For more information about KDE 3.5.5, see the KDE 3.5.5 <a href="http://www.kde.org/info/3.5.5.php">info page</a>.</p>