---
title: 'KDE 4.3.4 is in ports'
date: 2009-12-01 00:00:00 
layout: post
---

<p>KDE 4.3.4 and Qt 4.5.3 have been committed to ports.</p><p>The KDE/FreeBSD team is proud to announce the release of KDE 4.3.4 for FreeBSD. The official KDE 4.3.4 notes can be found <a href="http://kde.org/announcements/announce-4.3.4.php">here</a>.</p><p>With this update, we also updated Qt and PyQt4 bindings.</p><p>We'd like to say thanks to all helpers, testers and submitters.</p><p>Happy updating!</p>