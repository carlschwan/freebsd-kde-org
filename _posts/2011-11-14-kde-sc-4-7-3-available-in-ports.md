---
title: 'KDE SC 4.7.3 available in ports'
date: 2011-11-14 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce the release of KDE Software Compilation 4.7.3 for FreeBSD. We invite you to read the <a href="http://kde.org/announcements/announce-4.7.3.php">original announcement</a>.</p><p>Differently from the last update (from 4.6.5 to 4.7.2), this is an incremental, minor version update which should not require big interventions or cause big changes for users.</p>