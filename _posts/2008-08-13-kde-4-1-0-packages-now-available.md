---
title: 'KDE 4.1.0 packages now available'
date: 2008-08-13 00:00:00 
layout: post
---

<p>Packages are now available for KDE 4.1.0 on the FreeBSD FTP servers. Users who don't wish to build from source can now use these to quickly have a KDE 4.1.0 desktop.</p>