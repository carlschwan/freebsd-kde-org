---
title: 'Calligra update in area51'
date: 2016-02-08 00:00:00 
layout: post
---

<p>
The <a href="https://www.calligra.org/news/calligra-2-9-11-released/">Calligra suite</a> has been updated to version 2.9.11 today. This is available in the unofficial ports repository <a href="area51.php">area51</a>, in trunk.
    </p>