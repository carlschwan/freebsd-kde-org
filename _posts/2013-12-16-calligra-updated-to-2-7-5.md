---
title: 'Calligra updated to 2.7.5'
date: 2013-12-16 00:00:00 
layout: post
---

<p>The Calligra suite has been updated to <a href="http://www.calligra.org/news/calligra-2-7-released">version 2.7.5</a>.</p>