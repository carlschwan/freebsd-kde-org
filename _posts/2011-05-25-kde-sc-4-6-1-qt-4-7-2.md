---
title: 'KDE SC 4.6.1, Qt 4.7.2...'
date: 2011-05-25 00:00:00 
layout: post
---

<p>What a big set of updates the one committed today by kde@! We are pleased to announce:
			<ul>
				<li>Qt 4.7.2;</li>
				<li>PyQt4 4.8.3;</li>
				<li><b>KDE SC 4.6.1</b>, featuring an improved ksysguardd on its own (<tt>sysutils/ksysguardd</tt>, with no dependencies) that can be used for remote monitoring of systems;</li>
				<li>KOffice 2.3.1 (yes, further upgrade of this is planned);</li>
				<li>KDevelop 4.2.0;</li>
				<li>a few other ports you&#8217;re probably not interested in, but that you really want to upgrade.</li>
			</ul>
		</p><p>Official KDE release announcement can be found <a href="http://kde.org/announcements/announce-4.6.1.php">here</a>.</p><p>As always, we&#8217;d like to say thanks to all helpers and submitters. Writing a list would probably leave someone out, so drop an eye on our <a href="https://mail.kde.org/mailman/listinfo/kde-freebsd">mailing list</a> if you want their names.</p>