---
title: 'Qt 5.6.2 landed'
date: 2016-10-28 00:00:00 
layout: post
---

<p>
The KDE-FreeBSD team is happy to announce that the
latest Qt LTS (Long-Term Support) version, Qt 5.6.2,
has been committed to the official FreeBSD ports tree.
This marks the next step towards importing KDE Frameworks 5
into the official tree.
Users of Qt5 ports can update as usual.
    </p>