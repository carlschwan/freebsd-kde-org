---
title: "Newer Plasma in Area51"
date: 2018-10-06 00:00:00
layout: post
---

The latest KDE Plasma releases, 5.13 and the upcoming 5.14,
need a newer libinput than is available in the official
FreeBSD ports tree. There is a branch <tt>plasma5-5.13</tt>
with the necessary updates applied, for users wishing to
use the latest Plasma releases from Area51.
