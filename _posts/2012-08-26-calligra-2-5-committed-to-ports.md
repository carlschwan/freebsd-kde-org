---
title: 'Calligra 2.5 committed to ports'
date: 2012-08-26 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce <b>version 2.5 of Calligra</b>, coming to the ports tree after a short testing period which showed it to be quite stable.</p><p>For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.5 is still the student or academic user. This version has a number of new features that will make it more suitable for these users.<br />
			The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.</p><p>As usual, detailed release notes can be found in the <a href="http://www.calligra.org/news/calligra-2-5-released">official announcement</a>. You're strongly invited to update, as this release also solves a <a href="http://www.vuxml.org/freebsd/aa4d3d73-ef17-11e1-b593-00269ef07d24.html">security issue</a>.</p><p>Meanwhile, new translations were added:</p><pre>
- editors/calligra-l10n-gl (Gallegan)
</pre>