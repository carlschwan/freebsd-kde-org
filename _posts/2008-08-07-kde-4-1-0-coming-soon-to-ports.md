---
title: 'KDE 4.1.0 coming soon to ports'
date: 2008-08-07 00:00:00 
layout: post
---

<p>KDE 4.1.0 is now undergoing final testing in preparation for committing to ports. The repocopy from KDE 3 has been made, but users should not attempt to install the KDE 4.1.0 ports until a HEADS UP notification has been posted to the project mailing list and the ports mailing list.</p>