---
title: "KDE Applications updated"
date: 2018-09-06 00:00:00
layout: post
---

      The first monthly bugfix release for KDE Applications 18.08
      has landed
      in the official ports tree. Users can update normally.
