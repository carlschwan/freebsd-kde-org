---
title: "KDE Frameworks 5.50 Landed"
date: 2018-09-17 00:00:00
layout: post
---

      The latest monthly version of KDE Frameworks is available
      in the official FreeBSD ports tree.
