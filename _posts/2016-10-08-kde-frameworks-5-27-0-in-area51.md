---
title: 'KDE Frameworks 5.27.0 in area51'
date: 2016-10-08 00:00:00 
layout: post
---

<p>
The <a href="https://www.kde.org/announcements/kde-frameworks-5.27.0.php">latest KDE Frameworks</a> release -- this month is 5.27 -- is available in the unofficial KDE-FreeBSD ports repository, <a href="https://freebsd.kde.org/area51.php">area51</a>. Users of the plasma5/ branch in that repository can update as usual. or from the bleeding-edge packages provided by the KDE-FreeBSD team.
    </p>